import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
import fetch from 'isomorphic-fetch';

const app = express();
app.use(cors());
app.use(bodyParser.json());
function parseURL (url)  {
  if (url) {
    url = url.replace(/\//g, ' ').trim();
    // console.log(url);
    const result = url.split(' ');
    // console.log(result);
    return result.slice(1);
  }
  return [];
};


const pcUrl = 'https://gist.githubusercontent.com/isuvorov/ce6b8d87983611482aac89f6d7bc0037/raw/pc.json';

let pc = {};
fetch(pcUrl)
  .then(async(res) => {
    pc = await res.json();
  })
  .catch(err => {
    console.log('Чтото пошло не так:', err);
  });
app.get('/3a/length', async(req, res) => {
  let outobj = pc;
  outobj = outobj['length'];
  res.status(200).json(outobj);
});
app.get('/3a/volumes', async(req, res) => {
  let hdds = pc.hdd;

  let outobj = {};
  if (hdds) {
    hdds.forEach(hdd => {
      const volume = hdd.volume;
      // if (typeof (volume) == 'number')
      let size = outobj[volume] || 0;
      size += hdd['size'];
      outobj[volume] = size;
    });
  }
  if (outobj) {
    Object.keys(outobj).forEach(key => {
      outobj[key] = outobj[key] + 'B';
    });
    res.status(200).json(outobj);
  }
  else
    res.status(404).send('Not found');
});
app.get('/3a/*?', async(req, res) => {
  try {
    console.log(req.originalUrl);
    const objArr = parseURL(req.originalUrl);
    console.log(objArr);
    let outobj = pc;
    objArr.every(obj => {
      if (obj ==='length'){
        return outobj = undefined;};
      outobj = outobj[obj];
      return outobj != undefined;
    });
    if (outobj != undefined)
      res.status(200).json(outobj);
    else if (outobj === null)
      res.status(200).send('null');
    else
      res.status(404).send('Not Found');
  } catch (err) {
    res.send(err);
  }
});

app.listen(3000, () => {
  console.log('Your app listening on port 3000!');
});
